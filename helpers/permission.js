const config = require('../config')
var db = require('../db')
const jwt = require('jsonwebtoken')

module.exports = {
  validateSerialNumber: function (req, res, next) {
    var serialNumber = req.headers['x-access-serial-number'];
    console.log("serialNumber", serialNumber) 

    let sql = `SELECT id, name FROM tbl_vending_machine  WHERE serial_number = ?` 
    db.get().query(sql, [serialNumber], function (err, rows, fields) {
        if (err) {
          return res.status(500).send(err);
        } else {
             if(!rows.length){
              return res.status(401).send({
                success: false,
                message: 'Serial number incorrect'
              });
             } 
             req.vendingMachine = rows[0];
             next();
        }
    }); 
  },
  permission: function (req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    // decode token 
    if (token) {
      // verifies secret and checks exp
      jwt.verify(token, config.secretKey, function (err, decoded) {
        if (err) {
          return res.status(401).send({
            success: false,
            message: 'Permission denied'
          });
        } else {
          // if everything is good, save to request for use in other routes
          req.currentUser = decoded;
          next();
        }
      });

    } else {

      // if there is no token
      // return an error
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });

    }
  }
}
