const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');
// const path = require('path');

const env = process.env.NODE_ENV || 'development';
const logDir = 'log';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${logDir}/%DATE%-server.log`,
    datePattern: 'YYYY-MM-DD'
});

const logger = createLogger({
    // change level if in dev environment versus production
    level: env === 'development' ? 'verbose' : 'info',
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
    transports: [
        // new transports.Console({
        //     level: 'info',
        //     format: format.combine(
        //         format.colorize(),
        //         format.printf(
        //             info => `${info.timestamp} ${info.level}: ${info.message}`
        //         )
        //     )
        // }),
        dailyRotateFileTransport
    ]
});

exports.error = function (message) {
    return logger.error(message);
}

exports.warn = function (message) {
    return logger.warn(message);
}

exports.info = function (message) {
    return logger.info(message);
}

exports.verbose = function (message) {
    return logger.verbose(message);
}

exports.debug = function (message) {
    return logger.debug(message);
}

exports.silly = function (message) {
    return logger.silly(message);
}
 