var moment = require('moment');

module.exports = {
  DateTimeNow: function () {
    return moment.utc().format("YYYY-MM-DD HH:mm:ss");
  }
}