
var request = require('request');
var config = require('../config.json')

module.exports = {
    pushNotificationLine: function (message) {
        return new Promise(function (resolve, reject) {
            const option = {
                method: 'POST',
                uri: 'https://notify-api.line.me/api/notify',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                auth: {
                    'bearer': config.LINE_NOTIFY_TOKEN
                },
                form: {
                    message: message
                }
            }
            request(option, function (error, response, body) {
                if (error) {
                    reject(error)
                }
                resolve(response)
            });
        });
    }
}




 