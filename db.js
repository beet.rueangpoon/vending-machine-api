var promise_mysql = require('promise-mysql');
var proxyMysqlDeadlockRetries = require('node-mysql-deadlock-retries');
var pool = null;

var retries = 5;      // How many times will the query be retried when the ER_LOCK_DEADLOCK error occurs
var minMillis = 1;    // The minimum amount of milliseconds that the system sleeps before retrying
var maxMillis = 100;  // The maximum amount of milliseconds that the system sleeps before retrying

exports.connect = function () {
    console.log("connect db!")

    const host = process.env.DATABASE_HOST
    const user = process.env.DATABASE_USER
    const password = process.env.DATABASE_PASSWORD
    const database = process.env.DATABASE_NAME
    console.log("host : ", host)
    console.log("user : ", user)
    console.log("password : ", password)
    console.log("database : ", database)
    pool = promise_mysql.createPool({
        connectionLimit: 500,
        host: host,
        user: user,
        password: password,
        database: database,
        timezone: 'utc'
    });

    pool.on('connection', function (connection) {
        proxyMysqlDeadlockRetries(connection, retries, minMillis, maxMillis);
    })
}

exports.get = function () {
    return pool;
}
