var express = require('express');
var path = require('path');
// var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors'); //add cors
var fileUpload = require('express-fileupload');
var compression = require('compression')
var db = require('./db');
require('dotenv').config()


var routes = require('./routes/index');
var file = require('./routes/file');
var user = require('./routes/user');
var product = require('./routes/product');
var inventory = require('./routes/inventory');
var vendingMachine = require('./routes/vending-machine'); 
var orders = require('./routes/orders');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.disable('etag');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(cors()); //add cors
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); //false ==> true
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/download', express.static(path.join(__dirname, 'uploads')))
app.use(fileUpload());
app.use(compression())

app.use('/', routes);

app.get('/*', function (req, res, next) {
    res.setHeader('Last-Modified', (new Date()).toUTCString());
    next();
});

app.use(file)
app.use(user)
app.use(product);
app.use(inventory)
app.use(vendingMachine); 
app.use(orders);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// Send query to SERVER to keep the connection alive
setInterval(function () {
    db.get().query('SELECT 1')
        .catch(function (err) {
            console.log(err)
        })
}, 5000);

module.exports = app;
