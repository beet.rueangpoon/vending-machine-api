const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const fs = require('fs');
// const path = require('path');

const env = process.env.NODE_ENV || 'development';
const logDir = 'log';

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const dailyRotateFileTransport = new transports.DailyRotateFile({
    filename: `${logDir}/%DATE%-server.log`,
    datePattern: 'YYYY-MM-DD'
});

const logger = createLogger({
    // change level if in dev environment versus production
    level: env === 'development' ? 'verbose' : 'info',
    format: format.combine(
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
    ),
    transports: [
        new transports.Console({
            level: 'info',
            format: format.combine(
                format.colorize(),
                format.printf(
                    info => `${info.timestamp} ${info.level}: ${info.message}`
                )
            )
        }),
        dailyRotateFileTransport
    ]
});

const parseStringParameter = (parameter) => {
    if (!parameter) {
        return ''
    }
    switch (typeof (parameter)) {
        case "string":
            return parameter;
        case "object":
            return JSON.stringify(parameter);
        default:
            return parameter;;
    }
}

exports.error = function (message, parameter = null) {
    let sting = parseStringParameter(parameter)
    return logger.error(message + sting);
}

exports.warn = function (message, parameter = null) {
    let sting = parseStringParameter(parameter)
    return logger.warn(message + sting);
}

exports.info = function (message, parameter = null) {
    let sting = parseStringParameter(parameter)
    return logger.info(message + sting);
}

exports.verbose = function (message, parameter = null) {
    let sting = parseStringParameter(parameter)
    return logger.verbose(message + sting);
}

exports.debug = function (message, parameter = null) {
    let sting = parseStringParameter(parameter)
    return logger.debug(message + sting);
}

exports.silly = function (message, parameter = null) {
    let sting = parseStringParameter(parameter)
    return logger.silly(message + sting);
}
