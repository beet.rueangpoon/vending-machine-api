-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: test-beet-two
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_inventory`
--

DROP TABLE IF EXISTS `tbl_inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vending_machine_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `create_date_time` datetime NOT NULL,
  `update_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_inventory`
--

LOCK TABLES `tbl_inventory` WRITE;
/*!40000 ALTER TABLE `tbl_inventory` DISABLE KEYS */;
INSERT INTO `tbl_inventory` VALUES (1,1,1,15,'2021-02-26 17:36:43','2021-02-26 17:38:48'),(2,1,1,10,'2021-02-26 18:09:23',NULL),(3,2,1,5,'2021-02-26 18:09:33',NULL),(4,1,1,20,'2021-02-28 08:21:06',NULL),(5,1,2,20,'2021-02-28 08:21:14',NULL),(6,1,3,20,'2021-02-28 08:21:26',NULL),(7,1,4,20,'2021-02-28 08:21:35',NULL),(8,1,5,15,'2021-02-28 08:35:56',NULL),(9,1,6,5,'2021-02-28 08:36:09',NULL);
/*!40000 ALTER TABLE `tbl_inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_orders`
--

DROP TABLE IF EXISTS `tbl_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vending_machine_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `create_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_orders`
--

LOCK TABLES `tbl_orders` WRITE;
/*!40000 ALTER TABLE `tbl_orders` DISABLE KEYS */;
INSERT INTO `tbl_orders` VALUES (1,1,1,5,'2021-02-27 14:43:17'),(2,1,1,3,'2021-02-27 14:44:56'),(3,1,1,2,'2021-02-27 14:57:56'),(4,1,1,3,'2021-02-27 15:13:42'),(5,1,1,2,'2021-02-27 15:17:11'),(6,1,1,2,'2021-02-27 15:17:19'),(7,1,1,1,'2021-02-27 15:17:37'),(8,1,1,1,'2021-02-27 15:27:55'),(9,1,1,1,'2021-02-27 18:26:13'),(10,1,1,1,'2021-02-27 18:27:16'),(11,1,1,1,'2021-02-27 18:30:08'),(12,1,1,1,'2021-02-27 19:29:22'),(13,1,1,2,'2021-02-27 19:29:56'),(14,1,1,1,'2021-02-28 08:35:10'),(15,1,1,2,'2021-02-28 08:42:10'),(16,1,3,3,'2021-02-28 08:45:35'),(17,1,6,1,'2021-02-28 08:47:05');
/*!40000 ALTER TABLE `tbl_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_product`
--

DROP TABLE IF EXISTS `tbl_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_product`
--

LOCK TABLES `tbl_product` WRITE;
/*!40000 ALTER TABLE `tbl_product` DISABLE KEYS */;
INSERT INTO `tbl_product` VALUES (1,'Coke','1614420182277-coke.png',28,0),(2,'Fanta Green','1614420327371-fanta_green.png',28,0),(3,'Fanta Orange','1614420443857-fanta_orange.png',28,0),(4,'Sprite','1614420545031-sprite.png',28,0),(5,'Pepsi','1614420639249-pepsi.png',28,0),(6,'Est Cola','1614420754334-est_cola.png',27,0),(7,'Coke No Sugar','1614498235844-coke_no_sugar.png',28,0);
/*!40000 ALTER TABLE `tbl_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (1,'Kittichai','Rueangpoon','beet','$2b$08$dn2jFExh7FB7tCMYJGKis.rAzKHSe2KIYPoSIU.bLrV8MtMkbW5Sa',0),(2,'admin','admin','admin','$2b$08$U/CdLoTe5295ob7TGoKlJeohMuqFcZChWgrfv7i.KNEFD2UPzuaRS',0);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_vending_machine`
--

DROP TABLE IF EXISTS `tbl_vending_machine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_vending_machine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `serial_number` varchar(18) NOT NULL,
  `location_latitude` varchar(45) DEFAULT NULL,
  `location_longitude` varchar(45) DEFAULT NULL,
  `update_date_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_vending_machine`
--

LOCK TABLES `tbl_vending_machine` WRITE;
/*!40000 ALTER TABLE `tbl_vending_machine` DISABLE KEYS */;
INSERT INTO `tbl_vending_machine` VALUES (1,'Vending Machine One','4elh35wcklmk4hmt','13.8123147','100.5858895','2021-02-27 14:43:17'),(2,'Vending Machine Two','4elh35doklmlz7ab',NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_vending_machine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'test-beet-two'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-28 17:13:59
