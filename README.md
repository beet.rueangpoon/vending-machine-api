# [vending-machine. API Server]


## Dependencies
```
npm install
```

## Run

```
# edit host config in config.json to server private IP

sudo npm install -g pm2
pm2 start npm --name "vending-machine-api" -- run start
server api available at http://localhost:4040/...
```

## License

 
