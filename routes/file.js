var express = require('express')
var fs = require('fs');

var app = express.Router();

function writeFileToServer(file) {
    return new Promise(function (resolve, reject) {
        let fileName = Date.now() + '-' + file.name.replace(/\s/g, "").replace(/,/g, "_")
        let rootPathName = `./public/uploads/`
        let pathName = `${rootPathName}/${fileName}`
        if (!fs.existsSync(rootPathName)) {
            fs.mkdirSync(rootPathName);
        }
        file.mv(pathName, function (error) {
            if (error) {
                reject(error)
            }
            resolve(fileName)
        });
    });
}

app.post('/api/upload/file', function (req, res) {
    if (!req.files) {
        res.status(200).send({ success: false, msg: 'not file upload' });
        return;
    }
    var fileList = []
    Object.keys(req.files).map(function (key, index) {
        fileList.push(req.files[key])
    });
    Promise.all(fileList.map(file => writeFileToServer(file)))
        .then(function (results) {
            res.status(200).send(results);
        })
        .catch(function (err) {
            console.log(err)
            res.status(500).send({ error: err });
        });
})


module.exports = app;