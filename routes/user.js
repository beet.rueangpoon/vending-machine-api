var express = require('express')
var router = express.Router()
var user = require('./middleware/user')
var { permission } = require('../helpers/permission');

router.route('/api/user')
    .get(permission.bind(), user.getList)
    .post(permission.bind(), user.create)

router.route('/api/user/:id')
    .get(permission.bind(), user.get)
    .put(permission.bind(), user.update)

router.route('/public/user/login')
    .post(user.userLogin)

module.exports = router