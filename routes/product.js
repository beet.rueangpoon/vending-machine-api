var express = require('express')
var router = express.Router()
var product = require('./middleware/product')
var { permission, validateSerialNumber } = require('../helpers/permission');

router.route('/api/product')
    .get(permission.bind(), product.getList)
    .post(permission.bind(), product.create)

router.route('/api/product/:id')
    .get(permission.bind(), product.get)
    .put(permission.bind(), product.update)

router.route('/vending/machine/product')
    .get(validateSerialNumber.bind(), product.getByVendingMachine)

module.exports = router