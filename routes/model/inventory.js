var mysql = require('mysql')
var config = require('../config/mysql')
var logger = require('../../logger')

module.exports = {
    inventory: { 
        getList: function () {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = `SELECT 
                                inventory.id AS "id",
                                vending_machine.name AS "vending_machine_name",
                                product.name AS "product_name",
                                inventory.quantity AS "quantity",
                                inventory.create_date_time AS "create_date_time"
                            FROM tbl_inventory inventory
                            LEFT JOIN tbl_vending_machine vending_machine
                            ON inventory.vending_machine_id = vending_machine.id
                            LEFT JOIN tbl_product product
                            ON inventory.product_id = product.id`
                let parameters = []
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_INVENTORY_LIST_ERROR',
                            msg: 'Database execute "SELECT_INVENTORY_LIST" error'
                        }
                        return reject(error)
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        get: function (id) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = 'SELECT * FROM tbl_inventory WHERE id = ?'
                let parameters = [id]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_INVENTORY_WHERE_ID_ERROR',
                            msg: 'Database execute "SELECT_INVENTORY_WHERE_ID" error'
                        }
                        return reject(error)
                    }
                    if (!results.length) {
                        let error = {
                            code: 'SELECT_INVENTORY_WHERE_ID_NOT_FOUND',
                            msg: 'Not found "SELECT_INVENTORY_WHERE_ID"'
                        }
                        return reject(error)
                    }
                    let result = results[0]
                    return resolve(result)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        create: function (vendingMachineId, productId, quantity, createDateTime) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let inventory = {
                    vending_machine_id: vendingMachineId,
                    product_id: productId,
                    quantity: quantity,
                    create_date_time : createDateTime
                }
                let sql = 'INSERT INTO tbl_inventory SET ?'
                let parameters = [inventory]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'INSERT_INVENTORY_ERROR',
                            msg: 'Database execute "INSERT_INVENTORY" error'
                        }
                        return reject(error)
                    }
                    return resolve({ inventoryId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        update: function (inventoryId, vendingMachineId, productId, quantity, updateDateTime) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let sql = `UPDATE tbl_inventory SET  
                            vending_machine_id = ?,
                            product_id = ?,
                            quantity = ?,
                            update_date_time = ?
                            WHERE id = ?`
                let parameters = [vendingMachineId, productId, quantity, updateDateTime, inventoryId]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'UPDATE_INVENTORY_ERROR',
                            msg: 'Database execute "UPDATE_INVENTORY" error'
                        }
                        return reject(error)
                    }
                    return resolve({ inventoryId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },

        getStockBalanceList: function (vendingMachineId) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = `SELECT 
                                product.id AS "id",
                                product.name AS "name",
                                product.image AS "image",
                                IFNULL(inventory.quantity,0) -  IFNULL(orders.quantity,0) AS "quantity" 
                            FROM  tbl_product product
                            LEFT JOIN (
                            SELECT 
                                product_id,
                                vending_machine_id,
                                SUM(quantity) AS "quantity"
                            FROM tbl_inventory
                            WHERE vending_machine_id = ?
                            GROUP BY product_id, vending_machine_id
                            ) inventory
                            ON product.id = inventory.product_id
                            LEFT JOIN (
                            SELECT 
                                product_id,
                                vending_machine_id,
                                SUM(quantity) AS "quantity"
                            FROM tbl_orders
                            WHERE vending_machine_id = ?
                            GROUP BY product_id, vending_machine_id
                            ) orders
                            ON product.id = orders.product_id 
                            AND inventory.vending_machine_id = orders.vending_machine_id
                            ORDER BY product.id`
                let parameters = [vendingMachineId, vendingMachineId]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_STOCK_BALANCE_LIST_ERROR',
                            msg: 'Database execute "SELECT_STOCK_BALANCE_LIST" error'
                        }
                        return reject(error)
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
    }
}
