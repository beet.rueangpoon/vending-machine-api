var mysql = require('mysql')
var config = require('../config/mysql')
var logger = require('../../logger')

module.exports = {
    product: {
        getList: function (deleted) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = 'SELECT * FROM tbl_product'
                let parameters = []
                console.log('deleted', deleted)
                if (deleted == 0 || deleted == 1) {
                    sql += ' WHERE deleted = ?'
                    parameters = [deleted]
                }
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_PRODUCT_LIST_ERROR',
                            msg: 'Database execute "SELECT_PRODUCT_LIST" error'
                        }
                        return reject(error)
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        get: function (id) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = 'SELECT * FROM tbl_product WHERE id = ?'
                let parameters = [id]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_PRODUCT_WHERE_ID_ERROR',
                            msg: 'Database execute "SELECT_PRODUCT_WHERE_ID" error'
                        }
                        return reject(error)
                    }
                    if (!results.length) {
                        let error = {
                            code: 'SELECT_PRODUCT_WHERE_ID_NOT_FOUND',
                            msg: 'Not found "SELECT_PRODUCT_WHERE_ID"'
                        }
                        return reject(error)
                    }
                    let result = results[0]
                    return resolve(result)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        create: function (name, image, price, deleted) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let product = {
                    name: name,
                    image: image,
                    price: price,
                    deleted: deleted
                }
                let sql = 'INSERT INTO tbl_product SET ?'
                let parameters = [product]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'INSERT_PRODUCT_ERROR',
                            msg: 'Database execute "INSERT_PRODUCT" error'
                        }
                        return reject(error)
                    }
                    return resolve({ productId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        update: function (productId, name, image, price, deleted) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let sql = `UPDATE tbl_product SET  
                            name = ?,
                            image = ?,
                            price = ?,
                            deleted = ?
                            WHERE id = ?`
                let parameters = [name, image, price, deleted, productId]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'UPDATE_PRODUCT_ERROR',
                            msg: 'Database execute "UPDATE_PRODUCT" error'
                        }
                        return reject(error)
                    }
                    return resolve({ productId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        getByVendingMachine: function (vendingMachineId) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = `SELECT 
                                product.id AS "id",
                                product.name AS "name",
                                product.image AS "image",
                                IFNULL(inventory.quantity,0) -  IFNULL(orders.quantity,0) AS "quantity" 
                            FROM  tbl_product product
                            LEFT JOIN (
                            SELECT 
                                product_id,
                                vending_machine_id,
                                SUM(quantity) AS "quantity"
                            FROM tbl_inventory
                            WHERE vending_machine_id = ?
                            GROUP BY product_id, vending_machine_id
                            ) inventory
                            ON product.id = inventory.product_id
                            LEFT JOIN (
                            SELECT 
                                product_id,
                                vending_machine_id,
                                SUM(quantity) AS "quantity"
                            FROM tbl_orders
                            WHERE vending_machine_id = ?
                            GROUP BY product_id, vending_machine_id
                            ) orders
                            ON product.id = orders.product_id 
                            AND inventory.vending_machine_id = orders.vending_machine_id
                            ORDER BY product.id`
                let parameters = [vendingMachineId, vendingMachineId]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_PRODUCT_LIST_ERROR',
                            msg: 'Database execute "SELECT_PRODUCT_LIST" error'
                        }
                        return reject(error)
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
    }
}
