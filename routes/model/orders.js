var mysql = require('mysql')
var db = require('../../db')
var config = require('../config/mysql')
var logger = require('../../logger')
var { pushNotificationLine } = require('../../helpers/notification')

module.exports = {
    orders: {
        getList: function () {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = `SELECT 
                                orders.*, 
                                vending_machine.name AS "vending_machine_name",
                                product.name AS "product_name"
                            FROM tbl_orders orders
                            LEFT JOIN tbl_vending_machine vending_machine
                            ON orders.vending_machine_id = vending_machine.id
                            JOIN tbl_product product
                            ON orders.product_id = product.id`
                let parameters = []
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_ORDERS_LIST_ERROR',
                            msg: 'Database execute "SELECT_ORDERS_LIST" error'
                        }
                        return reject(error)
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
 
        createByVendingMachine: function (vendingMachineId, productId, quantity, createDateTime) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let orders = {
                    vending_machine_id: vendingMachineId,
                    product_id: productId,
                    quantity: quantity,
                    create_date_time: createDateTime,
                }
                let sql = `INSERT INTO tbl_orders SET ?`
                let parameters = [orders]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'INSERT_ORDERS_ERROR',
                            msg: 'Database execute "INSERT_ORDERS" error'
                        }
                        return reject(error)
                    }
                    return resolve({ ordersId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        checkQuantityProduct: function (vendingMachineId, vendingMachineName,productId) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = `SELECT 
                                product.id AS "id",
                                product.name AS "name",
                                IFNULL((inventory.quantity - orders.quantity),0) AS "quantity"
                            FROM  tbl_product product
                            LEFT JOIN (
                            SELECT 
                                product_id,
                                vending_machine_id,
                                SUM(quantity) AS "quantity"
                            FROM tbl_orders
                            GROUP BY product_id, vending_machine_id
                            ) orders
                            ON product.id = orders.product_id
                            LEFT JOIN (
                            SELECT 
                                product_id,
                                vending_machine_id,
                                SUM(quantity) AS "quantity"
                            FROM tbl_inventory
                            GROUP BY product_id, vending_machine_id
                            ) inventory
                            ON product.id = inventory.product_id
                            AND orders.vending_machine_id = inventory.vending_machine_id
                            AND orders.vending_machine_id = ?
                            WHERE product.id = ?`
                            let parameters = [vendingMachineId, productId]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_ORDERS_LIST_ERROR',
                            msg: 'Database execute "SELECT_ORDERS_LIST" error'
                        }
                        return reject(error)
                    }
                    if(results.length){
                        let product = results[0]
                        if(product.quantity < 10){
                            let message = `${vendingMachineName} : "${product.name}" has a quantity ${product.quantity} units`
                            pushNotificationLine(message)
                        }
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
    }
}
