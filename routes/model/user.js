var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var mysql = require('mysql')
var config = require('../config/mysql')
var logger = require('../../logger')
var configFile = require('../../config.json')
var { omit } = require('lodash')

function createToken(user) {
    return jwt.sign(omit(user, 'password'), configFile.secretKey, {
        expiresIn: 60 * 60 * 8
    });
}

module.exports = {
    user: {
        getList: function (deleted) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let sql = `SELECT * FROM tbl_user`
                let parameters = []
                if (deleted) {
                    sql += ' WHERE deleted = ?'
                    parameters = [deleted]
                }
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_USER_LIST_ERROR',
                            msg: 'Database execute "SELECT_USER_LIST" error'
                        }
                        return reject(error)
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        get: function (id) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = 'SELECT * FROM tbl_user WHERE id = ?'
                let parameters = [id]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_USER_WHERE_ID_ERROR',
                            msg: 'Database execute "SELECT_USER_WHERE_ID" error'
                        }
                        return reject(error)
                    }
                    if (!results.length) {
                        let error = {
                            code: 'SELECT_USER_WHERE_ID_NOT_FOUND',
                            msg: 'Not found "SELECT_USER_WHERE_ID"'
                        }
                        return reject(error)
                    }
                    let result = results[0]
                    return resolve(result)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        create: function (firstname, lastname, username, password, deleted) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                var salt = bcrypt.genSaltSync(configFile.saltRounds);
                var hashPassword = bcrypt.hashSync(password, salt);
                let user = {
                    firstname: firstname,
                    lastname: lastname,
                    username: username,
                    password: hashPassword,
                    deleted: deleted,
                }
                let sql = 'INSERT INTO tbl_user SET ?'
                let parameters = [user]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'INSERT_USER_ERROR',
                            msg: 'Database execute "INSERT_USER" error'
                        }
                        return reject(error)
                    }
                    return resolve({ labelId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        update: function (userId, firstname, lastname, username, password, deleted) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let sql = `UPDATE tbl_user SET 
                            firstname = ?,
                            lastname = ?,
                            username = ?,
                            deleted = ? 
                            WHERE id = ?`
                let parameters = [firstname, lastname, username, deleted, userId]
                if (password) {
                    var salt = bcrypt.genSaltSync(configFile.saltRounds);
                    var hashPassword = bcrypt.hashSync(password, salt);
                    sql = `UPDATE tbl_user SET 
                            firstname = ?,
                            lastname = ?,
                            username = ?,
                            password = ?,
                            deleted = ? 
                            WHERE id = ?`
                    parameters = [firstname, lastname, username, hashPassword, deleted, userId]
                }

                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'UPDATE_USER_ERROR',
                            msg: 'Database execute "UPDATE_USER" error'
                        }
                        return reject(error)
                    }
                    return resolve({ projectId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },


        userLogin: function (username, password) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let sql = `SELECT * FROM tbl_user WHERE username = ? AND deleted = 0`
                let parameters = [username]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_USER_WHERE_USERNAME_ERROR',
                            msg: 'Database execute "SELECT_USER_WHERE_USERNAME" error'
                        }
                        return reject(error)
                    }
                    if (!results.length) {
                        let error = {
                            code: 'SELECT_USER_WHERE_USERNAME_NOT_FOUND',
                            msg: 'Not found "SELECT_USER_WHERE_USERNAME"'
                        }
                        return reject(error)
                    }
                    let result = results[0]
                    if (!bcrypt.compareSync(password, result.password)) {
                        // console.log("Wrong Credential") 
                        let error = {
                            code: 'PASSWORD_IS_INCORRECT',
                            msg: 'Password is incorrect'
                        }
                        return reject(error)
                    }
                    delete result.password
                    let token = createToken(result)
                    result['token'] = token
                    return resolve(result)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
    }
}
