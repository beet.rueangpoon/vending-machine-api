var mysql = require('mysql')
var config = require('../config/mysql')
var logger = require('../../logger')

module.exports = {
    vendingMachine: {
        getList: function () {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = `SELECT * FROM tbl_vending_machine`
                let parameters = []
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_VENDING_MACHINE_LIST_ERROR',
                            msg: 'Database execute "SELECT_VENDING_MACHINE_LIST" error'
                        }
                        return reject(error)
                    }
                    return resolve(results)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        get: function (id) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })

                let sql = 'SELECT * FROM tbl_vending_machine WHERE id = ?'
                let parameters = [id]
                connection.query(sql, parameters, function (err, results, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'SELECT_VENDING_MACHINE_WHERE_ID_ERROR',
                            msg: 'Database execute "SELECT_VENDING_MACHINE_WHERE_ID" error'
                        }
                        return reject(error)
                    }
                    if (!results.length) {
                        let error = {
                            code: 'SELECT_VENDING_MACHINE_WHERE_ID_NOT_FOUND',
                            msg: 'Not found "SELECT_VENDING_MACHINE_WHERE_ID"'
                        }
                        return reject(error)
                    }
                    let result = results[0]
                    return resolve(result)
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        create: function (name, serialNumber) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let vendingMachine = {
                    name: name,
                    serial_number: serialNumber
                }
                let sql = 'INSERT INTO tbl_vending_machine SET ?'
                let parameters = [vendingMachine]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'INSERT_VENDING_MACHINE_ERROR',
                            msg: 'Database execute "INSERT_VENDING_MACHINE" error'
                        }
                        return reject(error)
                    }
                    return resolve({ vendingMachineId: result.insertId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        update: function (vendingMachineId, name) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let sql = `UPDATE tbl_vending_machine SET
                            name = ?
                            WHERE id = ?`
                let parameters = [name, vendingMachineId]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'UPDATE_VENDING_MACHINE_ERROR',
                            msg: 'Database execute "UPDATE_VENDING_MACHINE" error'
                        }
                        return reject(error)
                    }
                    return resolve({ vendingMachineId: vendingMachineId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
        updateLocation: function (vendingMachineId, locationLatitude, locationLongitude, updateDateTime) {
            return new Promise(function (resolve, reject) {
                let connection = mysql.createConnection(config.dbConnectionConfig())
                connection.connect(function (err) {
                    if (err) {
                        let error = {
                            code: 'DATABASE_CONNECTION_ERROR',
                            msg: 'Database connection error'
                        }
                        return reject(error)
                    }
                    logger.info('Connection established')
                })
                let sql = `UPDATE tbl_vending_machine SET
                            location_latitude = ?,
                            location_longitude = ?,
                            update_date_time = ?
                            WHERE id = ?`
                let parameters = [locationLatitude, locationLongitude, updateDateTime, vendingMachineId]
                connection.query(sql, parameters, function (err, result, fields) {
                    if (err) {
                        logger.error("error : ", err)
                        let error = {
                            code: 'UPDATE_VENDING_MACHINE_LOCATION_ERROR',
                            msg: 'Database execute "UPDATE_VENDING_MACHINE_LOCATION" error'
                        }
                        return reject(error)
                    }
                    return resolve({ vendingMachineId: vendingMachineId })
                })

                connection.end(function (error) {
                    if (error) {
                        logger.error("Terminate database connection error : ", error)
                    }
                })
            })
        },
    }
}
