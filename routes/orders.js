var express = require('express')
var router = express.Router()
var orders = require('./middleware/orders')
var { permission, validateSerialNumber } = require('../helpers/permission');

router.route('/api/orders')
    .get(permission.bind(), orders.getList)

router.route('/vending/machine/orders')
    .post(validateSerialNumber.bind(), orders.createByVendingMachine)

module.exports = router