exports.dbConnectionConfig = function () {
    return {
        host: process.env.DATABASE_HOST,
        user: process.env.DATABASE_USER,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
        timezone: 'utc',
        multipleStatements: true,
        charset: 'utf8_unicode_ci'
    }
}
