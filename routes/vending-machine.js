var express = require('express')
var router = express.Router()
var vendingMachine = require('./middleware/vending-machine')

router.route('/api/vending/machine')
    .get(vendingMachine.getList)
    .post(vendingMachine.create)

router.route('/api/vending/machine/:id')
    .get(vendingMachine.get)
    .put(vendingMachine.update)

router.route('/vending/machine/vending/machine/location')
    .post(vendingMachine.updateVendingMachineLocation) 

module.exports = router