var express = require('express')
var router = express.Router()
var inventory = require('./middleware/inventory')
var { permission } = require('../helpers/permission');

router.route('/api/inventory')
    .get(permission.bind(), inventory.getList)
    .post(permission.bind(), inventory.create)

router.route('/api/inventory/:id')
    .get(permission.bind(), inventory.get)
    .put(permission.bind(), inventory.update)

router.route('/api/stock/balance')
    .get(permission.bind(), inventory.getStockBalanceList)

module.exports = router