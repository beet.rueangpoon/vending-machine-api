var model = require('../model/user')
var { body, param, query, validationResult } = require('express-validator/check')
var { matchedData, sanitize } = require('express-validator/filter');

module.exports = {
    getList: [
        [
            query('deleted').isInt(),
            sanitize('deleted').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let deleted = data.deleted
            return model.user.getList(deleted)
                .then(function (results) {
                    return res.json(results)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    get: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let id = data.id
            return model.user.get(id)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    create: [
        [
            body('firstname').exists(),
            body('lastname').exists(),
            body('username').exists(),
            body('password').exists(),
            body('deleted').isInt(),
            sanitize('deleted').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let firstname = data.firstname
            let lastname = data.lastname
            let username = data.username
            let password = data.password
            let deleted = data.deleted

            model.user.create(firstname, lastname, username, password, deleted)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    update: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
            body('firstname').exists(),
            body('lastname').exists(),
            body('username').exists(),
            body('password').optional(),
            body('deleted').isInt(),
            sanitize('deleted').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let userId = data.id
            let firstname = data.firstname
            let lastname = data.lastname
            let username = data.username
            let password = data.password
            let deleted = data.deleted
            model.user.update(userId, firstname, lastname, username, password, deleted)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],

    userLogin: [
        [
            body('username').optional(),
            body('password').optional(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let username = data.username
            let password = data.password
            return model.user.userLogin(username, password)
                .then(function (result) {
                    return res.json({ success: true, user: result })
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error.code) {
                        case "PASSWORD_IS_INCORRECT":
                        case "SELECT_USER_WHERE_USERNAME_NOT_FOUND":
                            return res.status(200).send({ success: false, error: error });
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
}
