var model = require('../model/orders')
var { body, param, query, validationResult } = require('express-validator/check')
var { matchedData, sanitize } = require('express-validator/filter');
var { DateTimeNow } = require('../../helpers/datetime')
module.exports = {
    getList: [ 
        function (req, res) { 
            return model.orders.getList()
                .then(function (results) {
                    return res.json(results)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    createByVendingMachine: [
        [ 
            body('product_id').isInt(),
            sanitize('product_id').toInt(),
            body('quantity').isInt(), 
            sanitize('quantity').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let vendingMachineId = req.vendingMachine.id
            let vendingMachineName = req.vendingMachine.name
            let productId = data.product_id
            let quantity = data.quantity
            let createDateTime = DateTimeNow()
            return model.orders.createByVendingMachine(vendingMachineId, productId, quantity, createDateTime)
                .then(function (result) {
                    return model.orders.checkQuantityProduct(vendingMachineId,vendingMachineName, productId)
                })
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
}
