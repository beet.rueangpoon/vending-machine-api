var uniqid = require('uniqid');
var model = require('../model/vending-machine')
var { body, param, query, validationResult } = require('express-validator/check')
var { matchedData, sanitize } = require('express-validator/filter');
var { DateTimeNow } = require('../../helpers/datetime')

module.exports = {
    getList: [
        function (req, res) {
            return model.vendingMachine.getList()
                .then(function (results) {
                    return res.json(results)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    get: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let id = data.id
            return model.vendingMachine.get(id)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    create: [
        [
            body('name').exists(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let name = data.name
            let serialNumber = uniqid()
            model.vendingMachine.create(name, serialNumber)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    update: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
            body('name').exists(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let vendingMachineId = data.id
            let name = data.name 
            model.vendingMachine.update(vendingMachineId, name)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],  
    updateVendingMachineLocation: [
        [
            body('location_latitude').exists(),
            body('location_longitude').exists(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let vendingMachineId = req.vendingMachine.id 
            let locationLatitude = data.location_latitude 
            let locationLongitude = data.location_longitude
            let updateDateTime = DateTimeNow()
            model.vendingMachine.updateLocation(vendingMachineId, locationLatitude, locationLongitude, updateDateTime)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ], 
}
