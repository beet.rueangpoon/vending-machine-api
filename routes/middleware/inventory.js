var model = require('../model/inventory')
var { body, param, query, validationResult } = require('express-validator/check')
var { matchedData, sanitize } = require('express-validator/filter');
var {DateTimeNow} = require('../../helpers/datetime')

module.exports = {
    getList: [
        function (req, res) {
            return model.inventory.getList()
                .then(function (results) {
                    return res.json(results)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    get: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let id = data.id
            return model.inventory.get(id)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    create: [
        [
            body('vending_machine_id').isInt(),
            sanitize('vending_machine_id').toInt(),
            body('product_id').isInt(),
            sanitize('product_id').toInt(),
            body('quantity').isInt(),
            sanitize('quantity').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let vendingMachineId = data.vending_machine_id
            let productId = data.product_id
            let quantity = data.quantity 
            let createDateTime = DateTimeNow()
            model.inventory.create(vendingMachineId, productId, quantity, createDateTime)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    update: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
            body('vending_machine_id').isInt(),
            sanitize('vending_machine_id').toInt(),
            body('product_id').isInt(),
            sanitize('product_id').toInt(),
            body('quantity').isInt(),
            sanitize('quantity').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let inventoryId = data.id
            let vendingMachineId = data.vending_machine_id
            let productId = data.product_id
            let quantity = data.quantity 
            let updateDateTime = DateTimeNow()
            model.inventory.update(inventoryId, vendingMachineId, productId, quantity, updateDateTime)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    getStockBalanceList: [
        [
            query('vending_machine').isInt(),
            sanitize('vending_machine').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let vendingMachineId = data.vending_machine
            return model.inventory.getStockBalanceList(vendingMachineId)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
}
