var model = require('../model/product')
var { body, param, query, validationResult } = require('express-validator/check')
var { matchedData, sanitize } = require('express-validator/filter');

module.exports = {
    getList: [
        [
            query('deleted').isInt(),
            sanitize('deleted').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let deleted = data.deleted
            return model.product.getList(deleted)
                .then(function (results) {
                    return res.json(results)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    get: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let id = data.id
            return model.product.get(id)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    create: [
        [
            body('name').optional(),
            body('image').optional(),
            body('price').optional(),
            body('deleted').optional(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let name = data.name
            let image = data.image
            let price = data.price
            let deleted = data.deleted
            model.product.create(name, image, price, deleted)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    update: [
        [
            param('id').isInt(),
            sanitize('id').toInt(),
            body('name').optional(),
            body('image').optional(),
            body('price').optional(),
            body('deleted').optional(),
        ],
        function (req, res) {
            let data = matchedData(req)
            let productId = data.id
            let name = data.name
            let image = data.image
            let price = data.price
            let deleted = data.deleted
            model.product.update(productId, name, image, price, deleted)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
    getByVendingMachine: [
        function (req, res) {
            let VendingMachineId = req.vendingMachine.id
            return model.product.getByVendingMachine(VendingMachineId)
                .then(function (result) {
                    return res.json(result)
                })
                .catch(function (error) {
                    console.log(error)
                    switch (error) {
                        default:
                            return res.status(500).send({ errors: [error] })
                    }
                })
        }
    ],
}
